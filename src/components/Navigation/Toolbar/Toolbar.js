import React from 'react';

import classes from './Toolbar.module.css';
import Logo from "../../Logo/Logo";
import NavigationItems from "../NavigationItems/NavigationItems";
import Icon from '@material-ui/core/Icon';


const toolbar = (props) => (
    <header className={classes.Toolbar}>
        <Icon fontSize="large" onClick={props.drawerToggleClicked}>menu</Icon>
        <div className={classes.Logo}>
            <Logo/>
        </div>
        <nav className={classes.DesktopOnly}>
            <NavigationItems/>
        </nav>
    </header>
);

export default toolbar;
